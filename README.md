It ALL started from a question: How can I make it easier to make SURE we have digital copies of all these floppies we have at the Large Scale Systems Museum.


We have a LOT of floppies of various ages, vendors, formats, etc.  A LOT of software is already archived, but not necessarily easy to find who has a copy (or where to download it).  Still more is unarchived, or unknown.  We also don't know if we have ALL the versions, or good copies, etc.

Copyright being copyright, how do I know if someone ELSE has the software, or if mine is a variation that no one else has seen?  How do we detect variations of The Oregon Trail, where the software modifies the disk (therefore changing the hash of the data we'd use to otherwise uniquely identify versions).

So I developed a method to do a blockwise-hash of the image (of the data) - so you can see similarity between images, without having to go into EACH image file and do a compare.  Making it easier to identify variations of a common software.  [Prototype](https://gitlab.com/thinkulator/diskdiver/-/tree/main/research/fuzzy_sector_hash)

From there, I'm writing some automation software to handle the greaseweasel stuff, photos of the labels, and push post-processing onto a central server queue, that we can THEN analyze online or compare what we have vs other museums and collectors. 

The automation is basically a package format for the captured flux transitions, and the pictures of the front/back.  Gets put in a processing queue where it guesses at what kind of format it ACTUALLY is, and if it can figure it out - dumps the binary output, does a fuzzy hash, and adds it to the archive. [Prototype](https://gitlab.com/thinkulator/diskdiver/-/tree/main/Capture)

From there ( this is WAY in the future), the idea is that we can use some analysis tools to determine if we already have it, if it's a different version of something we already have, it's a data disk (and therefore interesting in a new way), or if it's parts of a software we don't have "ALL" of (Such as if we have a bad floppy, but we have ANOTHER floppy that's bad in a different way, but overlaps). 

We can also publish OUR list of hashes and photos (and OCR'd Label data) - so that other collectors and enthusiasts can find software we have in our archive and warehouse - without having to come dig themselves.
