const EventEmitter = require('node:events');
const Crypto = require('node:crypto');
const ChildProcess = require('node:child_process');
const FS = require('node:fs');

function generate_command(command,variables){
  let cmd = command
          .replaceAll("${SIDE}",variables.SIDE)
          .replaceAll("${UUID}",variables.UUID)
          .replaceAll("${TRACKS}",variables.TRACKS)
          .replaceAll("${GW_TTY_DEVICE}",variables.GW_TTY_DEVICE);
  return cmd;
}


class FloppyDrive extends EventEmitter {
  constructor(device,config,commands){
    super();
    this.device = device;
    this.config = config;
    this.commands = commands;
  }

   
  capture(uuid){
    this.uuid = uuid;
    this.status = "Starting...";
    this.sum_time = 0;
    let variables = {"SIDE":"front","UUID":this.uuid,"GW_TTY_DEVICE":this.device,"TRACKS":this.config.tracks};

//TODO: We should pass in the variables, and validate them here - instead of generating them here entirely
    let read_cmd = generate_command(this.commands.disk_fluxes_read_command,variables);

    if (!FS.existsSync('cap/'+this.uuid)) {
      FS.mkdirSync('cap/'+this.uuid);
    }

    this.gw_child = ChildProcess.spawn(read_cmd,[],{"shell":true});
    this.status = "Running...";

    const process_data = (data) => {
      let msg = data.toString();
      let header_match = msg.match(/^Reading\s+c=([0-9]+)-([0-9]+):h=([0-9]+)-([0-9]+) /i);
      let update_match = msg.match(/^T([0-9]+)\.([0-9]+): Raw Flux \(([0-9]+) flux in ([0-9]*\.[0-9]+)ms\)/i);
      
      if(header_match){
        this.tracks=Number(header_match[2])+1;
        this.heads=Number(header_match[4])+1;
        this.emit('start',this.tracks,this.heads);
      }else if(update_match){
        this.last_track = Number(update_match[1]);
  	    this.last_head = Number(update_match[2]);
        this.last_fluxes = Number(update_match[3]);
   	    this.last_time = Number(update_match[4]);
	    this.sum_time+=this.last_time;

        this.emit('status',this.last_track,this.last_head,this.last_fluxes,this.last_time);
      }else{
        console.log('Unexpected gw output: ',msg);
      }
      
    }

    let self = this;
    this.gw_child.on('exit', function (code) {
      if(code == 0){
        self.status = "Idle";
        self.emit("done");
      }else{
        self.status = "Idle";
        self.emit("error",'Child Process finished with exit code: '+code);
      }
    });

    this.gw_child.stdout.on('data', process_data);
    this.gw_child.stderr.on('data', process_data);
  }
}

module.exports = FloppyDrive;
