const EventEmitter = require('node:events');
const Crypto = require('node:crypto');
const ChildProcess = require('node:child_process');
const FS = require('node:fs');

function generate_command(command,variables){
    let cmd = command
        .replaceAll("${SIDE}",variables.SIDE)
        .replaceAll("${UUID}",variables.UUID)
        .replaceAll("${TRACKS}",variables.TRACKS)
        .replaceAll("${GW_TTY_DEVICE}",variables.GW_TTY_DEVICE);
    return cmd;
}

class Camera extends EventEmitter{

    constructor(commands){
        super();
        this.commands = commands;
    }

    capture(side,uuid,callback){
        this.uuid = uuid;
        this.side = side;

        let variables = {"SIDE":this.side,"UUID":this.uuid};

        if (!FS.existsSync('cap/'+this.uuid)) {
            FS.mkdirSync('cap/'+this.uuid);
        }

        this.status = "Running";
        this.emit('start');

        let capture_cmd = generate_command(this.commands.label_capture_command,variables);
        this.capture_child = ChildProcess.spawn(capture_cmd,[],{"shell":true});
        this.capture_child.callback = callback;

        let self = this;
        this.capture_child.on('exit', function (code) {
            self.capture_child.callback(code);

            if(code == 0){
                self.status = "Idle";
                self.emit("done");
            }else{
                self.status = "Idle";
                self.emit("error",'Child Process finished with exit code: '+code);
            }
        });

        const process_data = (data) =>{
          self.emit('data',data.toString());
        };

        this.capture_child.stdout.on('data', process_data);
        this.capture_child.stderr.on('data', process_data);
    }


}

module.exports = Camera;