#!/bin/bash
UUID=`uuidgen`


#Image Floppy Disk, before starting - specify type: 3.5” 5.25” 8”

mkdir -p "cap/$UUID"

#Align floppy to source marker plate (Ensure origin marker is visible for calibration).

#Take Picture of Front
read -N 1 -p "Ready to capture Front?  [Y/N]: " capture_ready
echo
if [ "${capture_ready^^}" = "N" ]; then
  echo "Quitting."
  exit
else
  echo "Capturing..."
  ffmpeg -loglevel 0 -f video4linux2 -s 1920x1200 -i /dev/video0 -ss 0:0:2 -frames 1 "cap/$UUID/front.jpg"
fi

#Take Picture of Rear
read -N 1 -p "Ready to capture Rear?  [Y/N]: " capture_ready
echo
if [ "${capture_ready^^}" = "N" ]; then
  echo "Quitting."
  exit
else
  echo "Capturing..."
  ffmpeg -loglevel 0 -f video4linux2 -s 1920x1200 -i /dev/video0 -ss 0:0:2 -frames 1 "cap/$UUID/rear.jpg"
fi


#Insert into Floppy Drive
read -N 1 -p "Insert floppy into drive.  Ready? [Y/N]: " capture_ready
echo
if [ "${capture_ready^^}" = "N" ]; then
  echo "Quitting."
  exit
else
  echo "Capturing..."
  #pull image of floppy
  gw read --device /dev/ttyACM0 "cap/$UUID/fluxes.scp"
fi


echo "Packaging as $UUID"
#package it up, and send it to the backend for post-processing
tar -cjf "cap/$UUID.tar.bz2" -C "cap/$UUID" $(ls "cap/$UUID")

rm -r "cap/$UUID"
