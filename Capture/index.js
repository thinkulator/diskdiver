const blessed = require('blessed');
const Crypto = require('node:crypto');
const config = require('./config.json');
const FloppyDrive = require('./floppydrive.js');
const FloppyPackager = require('./floppypackager.js');
const Camera = require('./camera.js');


let cam = new Camera(config.commands);


// Create a screen object.
var screen = blessed.screen({
  smartCSR: true
});

screen.title = 'Diskdiver Capture';

cam.on("error", (error) => {console.log('Cam error',error); });

function new_drive(drive,packager,layout,config){
  let box = blessed.form({
	  width: '50%', height: '30%',
	  tags: true, keys: true, autoNext: true,
	  border: { type: 'line'},
      label:drive.device,
      content:'\n      Status:\n        UUID:\nTracks/Heads:\nOperation:\nRemaining:',
	  style: { fg: 'white', bg: 'black', border: { fg: '#f0f0f0',bg: 'black' }, hover: { bg: 'green' }, focus: { bg: 'blue',border:{ bg: 'blue' }}}
	});
  //Initial UUID, the reset button sets this for future uses
  box.set("status","Idle");
  box.set("uuid",Crypto.randomUUID());
  box.set("status_front","  ");
  box.set("status_rear","  ");
  box.set("status_flux","  ");
  box.set("status_upload","  ");

  let status_input = blessed.input({
    parent: box, content: box.get("status"),
    padding: { left: 1, right: 1 }, left:13, top:1, width:'30%', height:1
  });

  let status_done_input = blessed.input({
    parent: box,
    padding: { left: 1, right: 1 }, right:0, top:1, width:'30%', height:1
  });

  let uuid_input = blessed.input({
    parent: box, content: box.get("uuid"),
    padding: { left: 1, right: 1 }, left:13, top:2, width:'70%', height:1
  });

  let tracksheads_input = blessed.input({
    parent: box, content: '',
    padding: { left: 1, right: 1 }, left:13, top:3, width:'20%',height:1
  });

  let operation_input = blessed.input({
    parent: box, content: '',
    padding: { left: 1, right: 1 }, left:13, top:4, width:'20%',height:1
  });

 let remaining_input = blessed.input({
    parent: box, content: '',
    padding: { left: 1, right: 1 }, left:13, top:5, width:'20%',height:1
  });


  function update_ui(){
      status_input.content = box.get("status");
      uuid_input.content = box.get("uuid");
     status_done_input.content =
         box.get("status_front")+" "+
         box.get("status_rear")+" "+
         box.get("status_flux")+" "+
         box.get("status_upload");
  }

  box.on('click', function(data) {
    box.focus();
    screen.render();
  });

  let front = blessed.button({
    parent: box,
    mouse: true, keys: true, shrink: true,
    padding: { left: 1, right: 1 }, left: 0, bottom: 2,
    name: 'front',
    content: 'Front',
    style: { bg: 'blue', focus: { bg: 'red' }, hover: { bg: 'red' } }
  });

  let rear = blessed.button({
    parent: box,
    mouse: true, keys: true, shrink: true,
    padding: { left: 1, right: 1 }, left: 10, bottom: 2,
    name: 'rear', content: 'Rear',
    style: { bg: 'blue', focus: { bg: 'red' }, hover: { bg: 'red' } }
  });

  let flux = blessed.button({
    parent: box,
    mouse: true, keys: true, shrink: true,
    padding: { left: 1, right: 1 }, left: 20, bottom: 2,
    name: 'flux', content: 'Flux',
    style: { bg: 'blue', focus: { bg: 'red' }, hover: { bg: 'red' } }
  });

  let upload = blessed.button({
    parent: box,
    mouse: true, keys: true, shrink: true,
    padding: { left: 1, right: 1 }, left:1, bottom: 0,
    name: 'upload',  content: 'Upload',
    style: { bg: 'blue', focus: { bg: 'red' }, hover: { bg: 'red' } }
  });

  let reset = blessed.button({
    parent: box,
    mouse: true, keys: true, shrink: true,
    padding: { left: 1, right: 1 }, right: 1, bottom: 0,
    name: 'reset',  content: 'Reset',
    style: { bg: 'blue', focus: { bg: 'red' }, hover: { bg: 'red' } }
  });

  function do_reset() {
    tracksheads_input.content ='';
    operation_input.content = '';

    box.set("status","Idle"); 
    box.set("uuid",Crypto.randomUUID());
    box.set("status_front","  ");
    box.set("status_rear","  ");
    box.set("status_flux","  ");
    box.set("status_upload","  ");
    update_ui();
  };


  upload.on('press', () => {
    box.set("status","Packaging..."); update_ui();
    packager.package(box.get("uuid"),() =>{
      box.set("status_upload","UP");
      box.set("status","Idle");

      do_reset();

      front.show();
      rear.show();
      flux.show();
      upload.show();

      update_ui();
      screen.render();
    });
    front.hide();
    rear.hide();
    flux.hide();
    upload.hide();
    screen.render();
  });

  front.on('press', () => {
    box.set("status","Capture Front..."); update_ui();
    camera_view.content='';
    cam.capture('front',box.get("uuid"),() => {
      box.set("status","Idle");
      box.set("status_front","FR");
      front.show();
      rear.show();
      flux.show();
      upload.show();

      update_ui();
      screen.render();
    });
    front.hide();
    rear.hide();
    flux.hide();
    upload.hide();
    screen.render();
  });

  rear.on('press', () => {
    box.set("status","Capture Rear..."); update_ui();
    camera_view.content='';
    cam.capture('rear',box.get("uuid"),() => {
      box.set("status_rear","RE");
      box.set("status","Idle");
      front.show();
      rear.show();
      flux.show();
      upload.show();
      update_ui();
      screen.render();
    });
    front.hide();
    rear.hide();
    flux.hide();
    upload.hide();
    screen.render();
  });

  flux.on('press', () => {
    box.set("status","Starting Flux Capture...");

    let uuid = box.get("uuid");
    drive.capture(uuid);

    flux.hide();
    front.hide();
    rear.hide();
    upload.hide();

    update_ui();
    screen.render();
  });
  
  reset.on('press', ()=> {
    do_reset();

    flux.show();
    front.show();
    rear.show();
    upload.show();

    update_ui();
    screen.render();
  });

  drive.on("start", (tracks,heads) => {
    box.set("status","Reading Fluxes..."); update_ui();
    /*box.content =
        "Tracks/Heads:  "+(drive.tracks)+"/"+(drive.heads)+
        "\nOperation: 0/"+(drive.tracks*drive.heads)+
        "\n"+drive.uuid;*/
    tracksheads_input.content = (drive.tracks)+"/"+(drive.heads);
    operation_input.content = "0/"+(drive.tracks*drive.heads);

    screen.render();
  });

  drive.on("status", (track,head,fluxes,time) => {
    box.set("status","Reading Fluxes..."); update_ui();
/*    box.content =
        "Tracks/Heads:  "+(drive.tracks)+"/"+(drive.heads)+
        "\nOperation: "+((track*drive.heads)+head)+"/"+(drive.tracks*drive.heads)+
        "\nElapsed:   "+(drive.sum_time/1000).toFixed(2)+"s"+
        "\nRemaining: "+(((drive.tracks*drive.heads)-(track*drive.heads)-head)*drive.last_time/1000).toFixed(2)+"s"+
        "\n"+drive.uuid;*/
    operation_input.content = ((track*drive.heads)+head)+"/"+(drive.tracks*drive.heads);
    //TODO: Actually estimate the time, the time sent from gw is not an accurate measure of the time it took, but of the floppy reading process.
    //remaining_input.content = (((drive.tracks*drive.heads)-(track*drive.heads)-head)*drive.last_time/1000).toFixed(2)+"s";
    screen.render();
  });

  drive.on("done", () => {
    box.set("status_flux","FL");
    box.set("status","Idle"); update_ui();
    front.show();
    rear.show();
    flux.show();
    upload.show();
    screen.render();
  });

  drive.on("error", (error) => {
    box.set("status","Error"); update_ui();
    console.log('Error',drive.device,error);
    screen.render();
  });

  update_ui();

  return box;
}

let layout = blessed.layout({
  parent: screen, keys: true,
  top: 'center', left: 'center', width: '100%', height: '100%',
  border: 'line', style: { bg: 'black', border: { fg: 'white' } }
});

let camera_view = blessed.box({
  parent:screen,
  content:'',
  width: '50%-1', height: '60%', right:'0', bottom:'0',
  tags: true, keys: true, autoNext: true,
  border: { type: 'line'},
  style: { fg: 'white', bg: 'black', border: { fg: '#f0f0f0',bg: 'black' }, hover: { bg: 'green' }, focus: { bg: 'blue',border:{ bg: 'blue' }}}
});

cam.on('data',(data) => {
  camera_view.content+=data;
});

let drive_boxes = [];
let drive_controllers = [];
let drive_packagers = [];
let drive_selected = -1;

// Append our box to the screen.
for(let drive_i in config.drives){
  let drive = new FloppyDrive(config.drives[drive_i].device,config.drives[drive_i].config,config.commands);
  drive_controllers.push(drive);
  packager = new FloppyPackager(config.commands);

  drive_boxes.push(new_drive(drive,packager,layout,{device:config.drives[drive_i].device}));

  packager.on('data',(data) => {
    console.log('Packager Data: ',data);
  });
  packager.on('error',(error) => {
    console.log('Packager Error',error);
  });
  drive_packagers.push(packager);
}


for(let a in drive_boxes){
  layout.append(drive_boxes[a]);
}

drive_boxes[0].focus();
drive_selected=0;


// Quit on Escape, q, or Control-C.
screen.key([/*'escape',*/ 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// The left and right arrow keys select the current drive
screen.key(['left','right'], function(ch, key) {
  if(key.name === 'right'){
    drive_selected++;
    drive_selected%=drive_boxes.length;
  }else if(key.name === 'left'){
    drive_selected--;
    if(drive_selected <0){
      drive_selected=drive_boxes.length-1;
    }
  }
  drive_boxes[drive_selected].focus();
  screen.render();
});



// Render the screen.
screen.render();
