const usb = require('usb');

// Get a list of all connected USB devices
const devices = usb.getDeviceList();

// Iterate through the devices and print their information
devices.forEach(device => {
  if(device.deviceDescriptor.idVendor == 0x239a){ //TODO: Fix this filter, figure out if it's a serial port (and if it's likely a greaseweazle)
    console.log('Bus: ',device.busNumber,'Address:',device.deviceAddress);
    console.dir(device.configDescriptor,{depth:9});


  }  
});
