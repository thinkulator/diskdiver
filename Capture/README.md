Diskdiver Capture is a CLI "curses" style app for capturing pictures and fluxes of A LOT of floppies in sequence, meant to be run on a dedicated station (Pi, desktop, etc), have a greaseweazle per-drive, and a webcam for caputring floppy/label pictures.

It is purely designed to capture the data, then package and submit for further processing on a central backend system (which could be on the same machine).

The package has the following format:
```
Filename: {UUID}.tar.bz2
Contents: fluxes.scp
          front.jpg
          rear.jpg (Optional)
```


From there, we send them to our Diskdiver.net server. Where it is decoded, processed, and archived.

The server can then process the label/front pictures, OCR their content to determine catalogging information such as Name, Sequence Number (1 of 2), OS, Version Number, or generally capture the whole label text for searching.
 

 **Running**
 ```
 #Edit the config.json file to match the appropriate TTY devices for your greaseweazles, and adjust the command lines for what programs you would like to use for image and flux captures.
 $node index.js
 ```

Press the buttons in the following order

* Front  (To capture a picture of the front of the floppy)
* Rear   (To capture a picture of the rear)
* Flux   (Capture the fluxes)
* Upload (Package the uuid's working directory contents for eventual sending to the Diskdiver.net backend system, sill being developed.)

The UUID is reset after upload, or can be reset at any time using the Reset button.  The code is not currently smart enough to stop a capture in the middle when you press reset.

**WARNING:**
This codebase is still alpha quality, and is riddled with gotchas and bugs.  The documentation is sparse, but improving.