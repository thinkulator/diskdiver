const Crypto = require('node:crypto');
let config = require('./config.json');
const FloppyDrive = require('./floppydrive.js');

/*  config.drives = {
        "A":"/dev/ttyACM0",
        "B":"/dev/ttyACM1"  } */

//  Place floppy in view of camera, confirm.
//  Capture the front side
//  Flip Over the floppy, confirm.
//  Capture the rear side
//  System picks an available floppy drive
//    User places floppy in drive, confirm.
//    Start capturing the fluxes on that drive.
//    Monitor completion and estimate time with each update.
//  Finish flux capure
//    Package the fluxes and images
//    Remove the uuid working directory
//    Transmit the package to the server
//    Remove the package from local storage.
//  Confirm removal of floppy from drive.

let c_dev = new FloppyDrive('/dev/ttyACM0',config);

c_dev.on("start", (tracks,heads) => {console.log('Start',c_dev.device,tracks,heads); });
c_dev.on("status", (track,head,fluxes,time) => {
	console.log('Status',
		c_dev.device,
		track,
		head,
		fluxes,
		time,
		"Current:"+((track*c_dev.heads)+head),
		"Of:"+(c_dev.tracks*c_dev.heads),
		"Elapsed:"+(c_dev.sum_time),
		"Remaining:"+((c_dev.tracks*c_dev.heads)-(track*c_dev.heads)-head)*c_dev.last_time
	); 
});
c_dev.on("complete", () => {console.log('Complete',c_dev.device); });
c_dev.on("error", (error) => {console.log('Error',c_dev.device,error); });
c_dev.on("user_input", (question,callback) => {console.log('User Input',c_dev.device,question); callback('Y');});

c_dev.capture(Crypto.randomUUID());


//this.emit('user_input','Ready for Front side photo?',(response) => {console.log('User Response:',response)});
