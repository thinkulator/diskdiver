const {createHash} = require('node:crypto');
const fs = require('node:fs');

if(typeof(process.argv[2]) === 'undefined'){
  console.log("program takes 1 argument, the name of the file to fuzzy sector hash");
  process.exit();
}


const raw = fs.readFileSync(process.argv[2]);
let fuzzy_sector_hash = '';

const logFile = process.argv[2]+".fush";
const pngFile = process.argv[2]+".fush.png";



const PNG = require('pngjs').PNG;

function bufferToPngImage(buffer, width, height, outputPath) {
  const png = new PNG({
    width: width,
    height: height,
    filterType: -1
  });

  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      const pixelIndex = (y * width + x) * 4;
      const byteIndex = (y * width + x) * 3; // 3 bytes per pixel

      png.data[pixelIndex] = buffer[byteIndex];     // Red
      png.data[pixelIndex + 1] = buffer[byteIndex + 1]; // Green
      png.data[pixelIndex + 2] = buffer[byteIndex + 2]; // Blue
      png.data[pixelIndex + 3] = 255; // Set alpha to fully opaque
    }
  }
  const pngWriterStream = fs.createWriteStream(outputPath);

  png.pack().pipe(pngWriterStream)
    .on('finish', () => pngWriterStream.close());
}





for(var sector_num = 0;sector_num < raw.length/512;sector_num++){
   let sector_view = new DataView(raw.buffer,sector_num*512,512);
   const result = createHash('sha256');
   const crc32 = require('buffer-crc32');

   result.update(sector_view);
   const sig = result.digest('bin');
   const sigCRC = crc32(sig);

   const sigStr = Array.from(sig).map((b) => b.toString(16).padStart(2, "0")).join("");
   const sigCRCStr = Array.from(sigCRC).map((b) => b.toString(16).padStart(2, "0")).join("")

   fs.appendFileSync(logFile,sigStr+";"+sigCRCStr+";"+sector_num+"\n");

//   console.log(sigStr,sigCRCStr,sector_num);

   fuzzy_sector_hash+=sigCRCStr;
}
console.log(fuzzy_sector_hash,";sector_size:512;algo:sha256+crc;",process.argv[2]);



const fushBuffer = Buffer.from(fuzzy_sector_hash, 'hex');


const numBytes=fushBuffer.length;
bufferToPngImage(
	fushBuffer,
	16,
	(numBytes/(16*3))+(((numBytes%(16*3))>0)?1:0),
	pngFile);

