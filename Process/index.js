const express = require('express');
const app = express();
const cors = require('cors');
const uuid = require('uuid');
const path = require('path');

const bz2 = require('unbzip2-stream');
const tar = require('tar-stream');
const fs = require('node:fs');

const storageserver = 'http://localhost:3000';
app.use(cors());

// Configure to accept plain text input
//app.use(express.text({ type: 'text/plain' }));

app.use('/archive', express.static('archive'));
app.use(express.static('public'));

/** Request the upload a new DiskDiver Flux Package, returns a redirect on where to ACTUALLY submit POST it to.
 *  This can be a local URL on the current server, or it can be a URL on a different one - potentially something like an S3 bucket.
 */
app.put('/api/v1/upload', (req, res) => {
   try{
     let requestID = req.header('X-Request-ID');
     if(typeof(requestID) === 'undefined' || requestID.trim().length == 0){
       //fill in a random request ID, since upstream proxy did not send us one.
       requestID = 'NO_REQ_HEADER_'+uuid.v4();
     }        
     requestID = requestID.replace(/[^A-z0-9\-\_]/g,'_');

     let uploaduuid = uuid.v4();
     res.redirect(storageserver+'/api/v1/upload/'+uploaduuid+'?signature=&for='+requestID);

     console.log('New Request Tracking ID:', requestID,'sending to','/api/v1/upload/'+uploaduuid);
   } catch (error) {
    console.log(error);
    res.type('text/plain').status(500).send(error.message);
   }
});

app.put('/api/v1/upload/:id', (req, res) => {
   try{
     let requestID = req.header('X-Request-ID');
     if(typeof(requestID) === 'undefined' || requestID.trim().length == 0){
       //fill in a random request ID, since upstream proxy did not send us one.
       requestID = 'NO_REQ_HEADER_'+uuid.v4();
     }        
     requestID = requestID.replace(/[^A-z0-9\-\_]/g,'_');
     console.log('Continue: ',requestID,' Continuation from',req.query.for);

     let uploadid = req.params.id;
     //TODO: Verify the request signature matches, once we add it to the requests that is.

     let extract = tar.extract();

     let files = [];

     extract.on('entry', function (header, stream, next) {
       files.push(header);

       stream.on('end', function () {
          next()
       });

       stream.resume() // AAAAAND, Go!
     });

     extract.on('finish', function () {
       console.log('Continue: ',requestID, 'Archive Complete');
     });
     let bz = bz2();

     //write the file to the archive dir
     //also stream it to a bzip decoder, and tar decompressor, so we can check it for correctness.
     req.pipe(fs.createWriteStream('archive/'+uploadid+'.tar.bz2'));
     req.pipe(bz).pipe(extract);

     let failed = false;
     req.on('end',()=> {
       if(failed){
         return;
       }
	  //check that there is a fluxes.scp file: 
          if(files.filter((e) => { return e.name === 'fluxes.scp' }).length == 0){
            res.status(400).send('Archive Missing fluxes.  See DiskDiver.net for Flux Package descriptor.');
          }else{
            res.status(200).send('Accepted');
          }
        });

     bz.on('error',(err)=> {
       failed=true;
       console.log(requestID,'Bzip Error',err.message);
       res.status(400).send('Decompression Error: '+err.message);
     });

   } catch (error) {
    console.log('Upload Failed',error);
    res.type('text/plain').status(500).send(error.message);
   }
});




const port = 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

