#!/bin/bash -e

ARCHIVE_TO_PROCESS="$1"

UUID="`basename \"$ARCHIVE_TO_PROCESS\" |cut -d. -f 1`"
WORKDIR="working/$UUID"



echo "Processing $ARCHIVE_TO_PROCESS as $UUID"

mv "$ARCHIVE_TO_PROCESS" "working/"
mkdir -p "$WORKDIR"

#TODO SECURITY: Prevent directory expansion, just unpack EVERYTHING in the working directory only
tar -xjvf "working/$UUID.tar.bz2" -C "$WORKDIR"


echo "Analyzing $UUID"
FORMAT_GUESS=`gw analyse  "$WORKDIR/fluxes.scp" |& tee "$WORKDIR/analyse.log" |grep "Best guess:" |cut -d: -f 2`

echo "Converting $UUID"
gw convert --format "$FORMAT_GUESS" "$WORKDIR/fluxes.scp" "$WORKDIR/decoded.img" |& tee "$WORKDIR/convert.log"


HASH_SUM=`sha256sum "$WORKDIR/decoded.img" |tee "$WORKDIR/sig.sha256"|cut -d\  -f 1`
echo "$UUID hash is $HASH_SUM"

echo "Cleaning Up"
rm "$WORKDIR/fluxes.scp"

echo "Done - Moving to archive "
mv "$WORKDIR" "archive/$UUID"
mv "working/$UUID.tar.bz2" "archive/$UUID/"

