const Sqlite3 = require("sqlite3");
const Readline = require("node:readline");
const process = require('node:process');

const rl = Readline.createInterface({ input: process.stdin });

const db = new Sqlite3.Database('archive/diver.db');
const insert_capture_stmt = db.prepare('insert into captures(id,fush,fush_algo) values (?,?,?);');


/** Example: ./1a4ffc66-7d57-45e5-907f-6d25d509f09d/decoded.img
    Returns: 1a4ffc66-7d57-45e5-907f-6d25d509f09d
*/
function process_path_to_uuid(path){
  const path_regex = /\/(.+)\/(.+)\.img/;
  let m = path_regex.exec(path);
  if(m){
	return m[1];
  }else{
        return;
  }
}

rl.on('line', (input) => {
  let [fush_txt,algo,file] = input.split(' ');
  const fush = Buffer.from(fush_txt, 'hex');
  let uuid = process_path_to_uuid(file);
  
  console.log(`Loading: ${uuid} ${algo} ${fush.length}`);
  insert_capture_stmt.run(uuid,fush,algo);
}); 




//TODO: Create DB if it does not currently exist






rl.on('close', () => {
  console.log('Closing');
  insert_capture_stmt.finalize();
  db.close();
});

